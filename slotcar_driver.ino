#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <PubSubClient.h>

/* Set these to your desired credentials. */
const char* ssid = "WLAN1-MMT0D5";
const char* password = "Windows7";
const char* mqtt_server = "192.168.8.100";

int motor_PWM = 0; //GPIO0
int fsrReading;

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];

void setup_wifi() {
   delay(100);
  // We start by connecting to a WiFi network
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) 
    {
      delay(500);
      Serial.print(".");
    }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) 
  {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    //if you MQTT broker has clientID,username and password
    //please change following line to    if (client.connect(clientId,userName,passWord))
    if (client.connect(clientId.c_str()))
    {
      Serial.println("connected");
     //once connected to MQTT broker, subscribe command if any
      client.subscribe("/slotcar/slot1/controller/throttle");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 6 seconds before retrying
      delay(6000);
    }
  }
} //end reconnect()

//void motionControl(){
//  if(motionActive){
//    if(actual_speed < target_speed){
//      actual_speed = actual_speed + acceleration_step;
//    } else if(actual_speed > target_speed){
//      actual_speed = actual_speed - acceleration_step;
//    }    
//    if(actual_speed > 1023){
//      actual_speed = 1023;
//    }
//    if(actual_speed < 0){
//      actual_speed = 0;
//    }
//    analogWrite(motor_PWM, actual_speed);
//  }  else {
//    analogWrite(motor_PWM, 0);
//  }
//}

void callback(char* topic, byte* payload, unsigned int length) 
{
//  String value = "";
//  for (int i=0;i<length;i++) {
//      value += (char)payload[i];
//  }
//  fsrReading = value.toInt();
//
//  analogWrite(motor_PWM, fsrReading);
//  Serial.println(fsrReading);
//
    String topicStr = topic;
    int payload_value;
    int updates_cal;
    if (length >= 2) {
      intValue = *((int*)payload);
    }
} //end callback

void setup() {
  pinMode(motor_PWM, OUTPUT);
  analogWrite(motor_PWM, 0);
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  client.subscribe("esp/test");
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  
  //Serial.println(message);
    

}
